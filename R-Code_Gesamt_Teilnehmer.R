# R für Linguisten
# Workshop an der Universität Regensburg (Institut für Anglistik und Amerikanistik --- Lehrstuhl für Englische Sprachwissenschaft)
# 24. März 2014
# Thomas BrunnerG:/r-workshop_data

# Der gesamte R-Code in dieser Datei stammt aus Dateien, die Stefan Gries' Lehrbuch "Statistik für Sprachwissenschaftler" (Göttingen: Vandenhoeck & Ruprecht 2008) beigegeben sind. Die Kapitel- und Abschnittsnummerierung bezieht sich auf die im Buch verwendete. 

#### Kapitel 1: Grundlagen von R

### Abschnitt 1.1: Einleitung und Installation
rm(list=ls(all=T)) # loesche den Speicher

### Abschnitt 1.2: Funktionen und Argumente
sqrt(5) 

a<-sqrt(5) 

a

(a<-sqrt(5))

rm(a) # loesche a

rm(list=ls(all=T)) 

x<-1:10 

sample(x, size=5, replace=T, prob=NULL)


sample(x, 5, T)

sample(x, 5)

x
sample(x)

sample(10)



## Abschnitt 1.3: Vektoren
# Abschnitt 1.3.1: Generierung von Vektoren in R

sqrt(5)

a<-sqrt(5)

length(a)


ein.name<-"Werner"; ein.name 

zahlen<-c(1, 2, 3); zahlen

namen<-c("anton", "berta", "caesar"); namen
(namen<-c("anton", "berta", "caesar"))

zahlen1<-c(1, 2, 3); zahlen2<-c(4, 5, 6) # generiere zwei Vektoren
zahlen1.und.zahlen2<-c(zahlen1, zahlen2) # verbinde die beiden Vektoren
zahlen1.und.zahlen2





# Abschnitt 1.3.2: Laden und Speichern von Vektoren in R

x<-scan(file="G:/r-workshop_data/_inputfiles/02-3-2_vector1.txt", sep="\n")
#x<-scan(file=file.choose(), sep="\n")
x


x<-scan(file=choose.files(), what="char", sep=" ", quiet=T) # und dann <G:/r-workshop_data/_inputfiles/02-3-2_vector2.txt> waehlen
#x<-scan(file=file.choose(), what="char", sep=" ", quiet=T)

x

setwd("G:/r-workshop_data")# Arbeitsverzeichnis setzen

cat(x, file="test-datei.txt") # Datei speichern
cat(x, file="test-datei.txt", sep="\n") 


# Abschnitt 1.3.3: Editieren von Vektoren in R

x<-c("a", "b", "c", "d", "e")
x[3] # greife auf das 3. Element von x zu

y<-3
x[y] # greife auf das 3. Element von x zu

z<-c(1, 3)
x[z] # greife auf das 1. und das 3. Element von x zu

z<-c(1:3)
x[z] # greife auf das 1. bis 3. Element von x zu

x=="d"

x<-c(10:1) 
x
x==4 

which(x==4) 
y <- which(x==4) 
x[y]


x[x==4]

x[x>8 | x<3]




# ÜBUNGEN --- Block I
# (1)   Generieren Sie einen Vektor m, der die kleinen Buchstaben von "a" bis "j" enthaelt (in alphabetischer Reihenfolge).

# (2)   Generieren Sie einen Vektor n, der die kleinen Buchstaben von "a" bis "j" enthaelt (in zufaelliger Reihenfolge).


# (3)   Speichern Sie den Vektor n in die Datei <G:/r-workshop_data/_outputfiles/02_exercises_vector3.txt>, so dass jeder Buchstabe in seiner eigenen Zeile steht.






## Abschnitt 1.5: Datensaetze 91
# Abschnitt 1.5.1: Generierung von Datensaetzen in R

rm(list=ls(all=T)) # loesche den Speicher

Wortart<-c("ADJ", "ADV", "N", "KONJ", "PRAEP")
TokenFrequenz<-c(421, 337, 1411, 458, 455)
TypenFrequenz<-c(271, 103, 735, 18, 37)
Klasse<-c("offen", "offen", "offen", "geschlossen", "geschlossen")

x<-data.frame(Wortart, TokenFrequenz, TypenFrequenz, Klasse)
x


x<-data.frame(TokenFrequenz, TypenFrequenz, Klasse, row.names=Wortart)
x


# Abschnitt 1.5.2: Laden und Speichern von Datensätzen in R

a<-read.table(file="G:/r-workshop_data/_inputfiles/02-5-2_dataframe1.txt", header=T, sep="\t", quote="", comment.char="") # ohne Zeilennamen: R numeriert

#a<-read.table(file.choose(), header=T, sep="\t", quote="", comment.char="")

a<-read.table(file="G:/r-workshop_data/_inputfiles/02-5-2_dataframe1.txt", header=T, sep="\t", quote="", comment.char="", row.names=1) # mit Zeilennamen: R numeriert nicht

#a<-read.table(file.choose(), header=T, sep="\t", quote="", comment.char="", row.names=1)

#Speichern
setwd("G:/r-workshop_data") 
write.table(a, file="G:/r-workshop_data/_outputfiles/02-5-2_dataframe2.csv", quote=F, sep="\t", col.names=NA)


# Abschnitt 1.5.3: Editieren von Tabellen in R
rm(list=ls(all=T)) # loesche den Speicher
a<-read.table(file="G:/r-workshop_data/_inputfiles/02-5-3_dataframe.txt", header=T, sep="\t", comment.char="", quote="")
#a<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")

a$TokenFrequenz
a$Klasse

ratio<-a$TokenFrequenz/a$TypenFrequenz; ratio

a[2,3] # der Wert in der 2. Zeile und der 3. Spalte
a[2,] # die Werte in der 2. Zeile, da die Spalte nicht spezifiziert wird
a[,3] # die Werte in der 3. Spalte, da die Zeile nicht spezifiziert wird
a[2:3,4] # zwei Werte der 4. Spalte
a[2:3,3:4] # die Werte der 2. und 3. Zeile der 3. und 4. Spalte


which(a[,2]>450) 
a[,3][which(a[,3]>100)] 

attach(a)

Klasse

Klasse[4]<-"NA"; Klasse

a

Klasse[4]<-"geschlossen"

b<-a[Klasse=="offen",]; b

b<-a[a[,4]=="offen",]; b


b<-subset(a, Klasse=="offen")




# ÜBUNGEN --- Block II

# (1)   Erzeugen Sie einen Datensatz abc, der in der ersten Spalte die Buchstaben von "a" bis "j" und in der zweiten Spalte die Zahlen von 10 bis 1 enthaelt. Die erste Spalte soll den Namen "BUCHSTABEN" haben, die zweite den Namen "ZAHLEN".

# (2)   Laden Sie die Textdatei <G:/r-workshop_data/_inputfiles/02_exercises_dataframe1.txt> in einen Datensatz mit dem Namen beispiel so, dass die erste Zeile fuer die Spaltennamen verwendet wird. Machen Sie die Daten in Spalten als Vektoren/Faktoren verfuegbar.

# (3)  Extrahieren Sie aus diesem Datensatz
#       (a) die Spalten 2 und 3

#       (b) die Zeilen 3 und 4

# (4)  Teilen Sie den Datensatz beispiel auf der Basis der Daten der zweiten Spalte auf.

# (5)  Aendern Sie den Wert der 3. Zeile und 4 Spalte in "indef" und speichern Sie den geaenderten Datensatz in die Datei <G:/r-workshop_data/_outputfiles/02_exercises_dataframe2.txt>, so dass die Datei leicht mit einer Tabellenkalkulation geoeffnet/bearbeitet werden kann.

# (6)  Erzeugen sie den folgenden Datensatz mit Namen EPP: [...]

# (7)  Extrahieren Sie aus diesem Datensatz
#       (a) den Wert in der vierten Zeile der zweiten Spalte

#       (b) die Werte der Zeilen 3 und 4 in den ersten beiden Spalten

#       (c) die Zeilen mit Pronomen im Plural

#       (d) die Zeilen mit Pronomen der ersten oder dritten Person


# (8)  Erzeugen Sie einen Vektor FREQS, der die Haeufigkeiten der Personalpronomen in EPP in einem kleinen Korpus enthaelt: ich: 8426, du: 9462, er: 6394, sie: 4234, es: 6040, wir: 2305, ihr: 8078, sie: 2998. Fuegen Sie diesen Vektor als vierte Spalte zu EPP hinzu.


# (9)  Speichern Sie diesen Datensatz in die Datei <G:/r-workshop_data/_outputfiles/02_exercises_dataframe3.txt>, so dass die Datei leicht mit einer Tabellenkalkulation geoeffnet/bearbeitet werden kann.





#### Kapitel 2: Deskriptive Statistik
### Abschnitt 2.1: Univariate Statistiken
rm(list=ls(all=T)) # loesche den Speicher

## Abschnitt 2.1.1: Haeufigkeitsdaten

AEHM<-read.table(file="G:/r-workshop_data/_inputfiles/03-1_aeh(m).txt", header=T, sep="\t", comment.char="", quote="")
#AEHM<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")


attach(AEHM)
str(AEHM)

table(FILLER) # generiere eine Haeufigkeitstabelle

prop.table(table(FILLER)) # generieren eine Tabelle relativer Haeufigkeiten



# 2.1.1.1 Punkt-/Streu- und Liniendiagramme
a<-c(1, 3, 5, 2, 4); b<-1:5
 
# png("G:/r-workshop_data/graph.png", width=300, height=300) # unkommentieren, um die Grafik in eine Datei zu speichern
plot(a) 
# dev.off() # unkommentieren, um das Speichern der Grafik in eine Datei abzuschliessen

# WICHTIG: Man kann alle Graphik-Ausgaben von R auch speichern: vgl. oben: png() und dev.off(). Auch möglich: pdf() oder bmp()


plot(a, b)
plot(b, a, type="b") 
plot(b, a, type="l") 

plot(b, a, xlab="Ein Vektor b", ylab="Ein Vektor a", xlim=c(0, 8), ylim=c(0, 8), type="b")
grid() 



# 2.1.1.3 Balkendiagramme

barplot(table(FILLER)) 

barplot(table(FILLER), col=c("grey20", "grey40", "grey60"),  names.arg=c("Aeh", "Aehm", "Stille")) 




# 2.1.1.5 Histogramme
hist(LAENGE) 

## Abschnitt 2.1.2: Masse der zentralen Tendenz
# Abschnitt 2.1.2.1: Der Modalwert
which.max(table(FILLER))

# Abschnitt 2.1.2.2: Der Median
median(LAENGE)

# Abschnitt 2.1.2.3: Das arithmetische Mittel
mean(LAENGE)


## Abschnitt 2.1.3 Dispersion und Streuungsmasse 

Stadt1<-c(-5, -12, 5, 12, 15, 18, 22, 23, 20, 16, 8, 1)
Stadt2<-c(6, 7, 8, 9, 10, 12, 16, 15, 11, 9, 8, 7)
mean(Stadt1) 
mean(Stadt2)

plot(Stadt1,type="b"); points(Stadt2, type="b"); abline(a=0,0)

# Abschnitt 3.1.3.3: Quantile und Quartile
a<-1:100 
quantile(a)
quantile(Stadt1)
IQR(Stadt1) # die Funktion fuer den Interquartilsabstand
quantile(Stadt2)
IQR(Stadt2)

# Abschnitt 2.1.3.2: Die Standardabweichung

Stadt1
Stadt1-mean(Stadt1)
(Stadt1-mean(Stadt1))^2
sum((Stadt1-mean(Stadt1))^2) # der Zaehler
sum((Stadt1-mean(Stadt1))^2)/(length(Stadt1)-1)
sqrt(sum((Stadt1-mean(Stadt1))^2)/(length(Stadt1)-1))

sd(Stadt1)
sd(Stadt2)


# Abschnitt 2.1.3.3: Der Variationskoeffizient

sd(Stadt1)
sd(Stadt1*10) 
sd(Stadt1)/mean(Stadt1) 
sd(Stadt1*10)/mean(Stadt1*10) 
sd(Stadt2)/mean(Stadt2)

# Abschnitt 2.1.3.4: Zusammenfassende Funktionen
summary(Stadt1)

boxplot(Stadt1, Stadt2, notch=T)




# ÜBUNGEN --- Block III
# (1)   Errechnen Sie fuer die folgenden Zahlen den Mittelwert: 1, 2, 4, 8, 10, 12.


# (2)   Errechnen Sie fuer den folgenden Klassenspiegel
#       1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6.

#       (a) einen Mittelwert

#       (b) ein Streuungsmass

#       (c) Stellen Sie das Ergebnis graphisch dar in einem Balkendiagramm.




### Abschnitt 2.2: Bivariate Statistiken
rm(list=ls(all=T)) # loesche den Speicher

## Abschnitt 2.2.1: Haeufigkeiten und Kreuztabellen
AEHM<-read.table(file="G:/r-workshop_data/_inputfiles/03-1_aeh(m).txt", header=T, sep="\t", comment.char="", quote="")
#AEHM<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")
attach(AEHM)
fix(AEHM)
werte<-table(FILLER, GESCHLECHT); werte

prozente<-prop.table(table(FILLER, GESCHLECHT), margin=2); prozente


ftable(GENRE, FILLER, GESCHLECHT)


# Abschnitt 2.2.1.1: Balkendiagramme und Mosaikplots

plot(FILLER~GESCHLECHT) 
plot (GESCHLECHT, FILLER)

x<-prop.table(table(FILLER, GESCHLECHT), margin=2)
barplot(x, col=c("red", "yellow", "green"), space=0, xlab="Geschlecht", ylab="Prozent")

text(0.5, x[1,1]/2, paste("aeh:", round(x[1,1], 3), sep="\n"))
text(0.5, x[1,1]+x[2,1]/2, paste("aehm:", round(x[2,1], 3), sep="\n"))
text(0.5, x[1,1]+x[2,1]+x[3,1]/2, paste("Stille:", round(x[3,1], 3), sep="\n"))
text(1.5, x[1,2]/2, paste("aeh:", round(x[1,2], 3), sep="\n"))
text(1.5, x[1,2]+x[2,2]/2, paste("aehm:", round(x[2,2], 3), sep="\n"))
text(1.5, x[1,2]+x[2,2]+x[3,2]/2, paste("Stille:", round(x[3,2], 3), sep="\n"))


## Abschnitt 2.2.2: Mittelwerte
mean(LAENGE[GESCHLECHT=="maennlich"])
mean(LAENGE[GESCHLECHT=="weiblich"])

tapply(LAENGE, GESCHLECHT, mean)


# Abschnitt 2.2.2.1: Boxplots
boxplot(LAENGE~GENRE, notch=T, ylim=c(0, 1600)); grid()



# ÜBUNGEN --- Block IV

# (1)   Zehn bilinguale Schueler nahmen an einem Deutschdiktat und einem Englischdiktat teil und erzielten dabei die folgenden Fehlerzahlen.
#       29, 20, 10, 16, 12, 15, 25, 22, 20, 23
#       21, 19, 28, 28, 26, 18, 16, 22, 20, 28

# Sie haben jetzt auch noch die Geschlechter der Schueler erhalten. Schueler 2 bis 6 waren Maedchen, der Rest Jungen.
#       (a) Geben Sie diese Daten in R ein.

#       (b) Errechnen Sie die mittleren Fehlerzahlen im Deutschdiktat fuer Maedchen und Jungen.

#       (c) Stellen Sie die Fehlerzahlen im Deutschdiktat in Abhaengigkeit vom Geschlecht graphisch dar.



# (2)  Die Datei <G:/r-workshop_data/_inputfiles/03_exercises.txt> enthaelt Korpusdaten zur Wortstellungsalternation der Partikelbewegung, die in Abschnitt 1.3.1 eingefuehrt wurde.
#       - Spalte 1: die Nummer des Beispiels/Datenpunkts
#       - Spalte 2: ob das Beispiel aus gesprochener oder geschriebener Sprache stammt
#       - Spalte 3: welche Konstruktion im Beispiel verwendet wurde
#       - Spalte 4: wie komplex das direkte Objekt war (3 Variablenauspraegungen)
#       - Spalte 5: wie lang das direkte Objekt war (gemessen in Silben)
#       - Spalte 6: ob der Verb-Partikel-Konstruktion eine direktionale PP folgt (2 Variablenauspraegungen)
#       - Spalte 7: ob der Referent des direkten Objekts belebt war (2 Variablenauspraegungen)
#       - Spalte 8: ob der Referent des direkten Objekts konkret war (2 Variablenauspraegungen)

#       (a) Lesen Sie diese Datei in R ein, machen Sie die Spaltennamen als Variablen verfuegbar und ueberpruefen Sie, ob die Daten korrekt eingelesen wurden.
#       (b) Stellen Sie den Zusammenhang zwischen der Konstruktionswahl und der Komplexitaet des direkten Objekts graphisch dar.

#       (c) Generieren Sie eine Tabelle, in der Sie den Zusammenhang zwischen der Konstruktionswahl und der Komplexitaet des direkten Objekts darstellen und fassen Sie das Ergebnis kurz zusammen.

#       (d) Stellen Sie den Zusammenhang zwischen der Konstruktionswahl und der Laenge des direkten Objekts graphisch dar und fassen Sie das Ergebnis kurz zusammen.
#       (e) Berechnen Sie statistisch, ob die Konstruktionswahl etwas mit der Laenge des direkten Objekts zu tun.

#       (f) Pruefen Sie, ob die Konstruktionsahl von sowohl der Belebtheit des Referenten des direkten Objekts als auch der An-/Abwesenheit einer direktionalen PP bedingt ist.





#Abschnitt 3: Analytische Statistik

# Abschnitt 3.1: eine abhaengige Variable (nominal/kategorial) und eine unabhaengige Variable (nominal/kategorial) (unabhaengige Stichproben): Chi-Quadrat-Unterschiedstest

# loesche den Speicher
rm(list=ls(all=T))

# lade die Daten
VPCs<-read.table(file="G:/r-workshop_data/_inputfiles/04-1-2-2_vpcs.txt", header=T, sep="\t", comment.char="", quote="")
#VPCs<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")

attach(VPCs); str(VPCs) # mach' die Spalten verfuegbar und zeige die Struktur der Daten an

plot(KONSTRUKTION~BEKANNTHEIT)
table(KONSTRUKTION, BEKANNTHEIT)

# rechne den Chi-Quadrat-Test mit R
chisq.test(table(KONSTRUKTION, BEKANNTHEIT), correct=F)

# ermittle die unter H0 zu erwartenden Häufigkeiten.
chisq.test(table(KONSTRUKTION, BEKANNTHEIT), correct=F)$exp

# rechne den Chi-Quadrat-Test und erhalte nur den Chi-Quadrat-Wert
chisq.test(table(KONSTRUKTION, BEKANNTHEIT), correct=F)$statistic

# rechne mit R den Chi-Quadrat-Test fuer eine 10mal so grosse Stichprobe mit der gleichen Verteilung
chisq.test(table(KONSTRUKTION, BEKANNTHEIT)*10, correct=F)


sqrt(chisq.test(table(KONSTRUKTION, BEKANNTHEIT), correct=F)$statistic/sum(table(KONSTRUKTION, BEKANNTHEIT))*(min(dim(table(KONSTRUKTION, BEKANNTHEIT)))-1))


# ermittle die Abweichungsmasse und Residuals
chisq.test(table(KONSTRUKTION, BEKANNTHEIT), correct=F)$residuals 
chisq.test(table(KONSTRUKTION, BEKANNTHEIT), correct=F)$residuals^2 


assocplot(table(KONSTRUKTION, BEKANNTHEIT), col=c("black", "darkgrey"))





# Abschnitt 3.2: eine abhaengige Variable (verhaeltnisskaliert) und eine unabhaengige Variable (nominal) (unabhaengige Stichproben): der t-Test fuer unabhaengige Stichproben



# loesche den Speicher
rm(list=ls(all=T))

# lade die Daten
Vokale<-read.table(file="G:/r-workshop_data/_inputfiles/04-3-2-1_f1-freq.txt", header=T, sep="\t", comment.char="", quote="")
#Vokale<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")

attach(Vokale); str(Vokale) # mach' die Spalten verfuegbar und zeige die Struktur der Daten an

mean(HZ_F1[GESCHLECHT=="M"])
mean(HZ_F1[GESCHLECHT=="W"])

tapply(HZ_F1, GESCHLECHT, mean)

boxplot(HZ_F1~GESCHLECHT, notch=T, ylim=c(0, 1000)); grid()


# rechne die Shapiro-Wilk-Tests: Pruefung der Normalverteilung
tapply(HZ_F1, GESCHLECHT, shapiro.test)

# pruefe die Voraussetzungen fuer den t-Test fuer unabhaengige Stichproben: Pruefung der Varianzenhomogenitaet
var.test(HZ_F1~GESCHLECHT) # Formelschreibweise

# rechne um den t-Test fuer unabhaengige Stichproben mit R
t.test(HZ_F1~GESCHLECHT, paired=F) # Formelschreibweise



# Abschnitt 3.3: eine abhaengige Variable (ordinal) und eine unabhaengige Variable (nominal) (unabh. Stichproben): der U-Test S. 220

# loesche den Speicher
rm(list=ls(all=T))

# lade die Daten
Dices<-read.table(file="G:/r-workshop_data/_inputfiles/04-3-2-3_dices.txt", header=T, sep="\t", comment.char="", quote="")

#Dices<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")

attach(Dices); str(Dices) # mach' die Spalten verfuegbar und zeige die Struktur der Daten an

# Mittelwerte und grafische Betrachtung

boxplot(DICE~PROZESS, notch=T, ylim=c(0, 1))

tapply(DICE, PROZESS, mean)

# Pruefung der Normalverteilung
par(mfrow=c(1,2))
	hist(DICE[PROZESS=="Blend"], main="", ylab="Haeufigkeit", xlim=c(0, 0.7), ylim=c(0,35))
	hist(DICE[PROZESS=="KomplKlip"], main="", ylab="Haeufigkeit", xlim=c(0, 0.7), ylim=c(0,35))
par( mfrow=c(1,1)) 

tapply(DICE, PROZESS, shapiro.test)

tapply(DICE, PROZESS, median)
tapply(DICE, PROZESS, IQR)

# rechne den U-Test mit R
wilcox.test(DICE~PROZESS, paired=F, correct=T) # Formelschreibweise


#### Kapitel 3.4: Die hierarchische Konfigurations-Frequenzanalyse

# loesche den Speicher
rm(list=ls(all=T))

VPCs<-read.table(file="G:/r-workshop_data/_inputfiles/05-1-1_vpcs.txt", header=T, sep="\t", comment.char="", quote="")
#VPCs<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")


attach(VPCs) # um die Spalten verfuegbar zu machen

# rechne den Chi-Quadrat-Test mit R
chisq.test(table(KONSTRUKTION, REFERENT), correct=F)

# inspiziere die Residualwerte
chisq.test(table(KONSTRUKTION, REFERENT), correct=F)$res^2

# loesche den Speicher
rm(list=ls(all=T))

# lade die Daten
Genitive<-read.table(file="G:/r-workshop_data/_inputfiles/05-1-1_genitives.txt", header=T, sep="\t", comment.char="", quote="")
#Genitive<-read.table(file.choose(), header=T, sep="\t", comment.char="", quote="")

attach(Genitive) # um die Spalten verfuegbar zu machen
str(Genitive) # um die Struktur der Daten anzuzeigen

table(GENITIV, POSSESSOR, POSSESSED)
table(POSSESSOR, POSSESSED, GENITIV)

ftable(POSSESSOR, POSSESSED, GENITIV)
ftable(GENITIV ~ POSSESSOR+POSSESSED) 

prop.table(ftable(GENITIV ~ POSSESSOR+POSSESSED), 1)


# Exkurs: graphische Darstellung der wichtigsten Interaktion in diesen Daten: POSSESSOR:GENITIV
# Ansatz 2: die y-Achse basiert auf den beobachteten Prozentwerten (besser)
# um die Variablenlevels von POSSESSOR zu veraendern (um eine links->rechts Skalierung in den Daten zu erreichen)
POSSESSOR<-factor(POSSESSOR, levels=c("konkret", "abstrakt", "belebt"))
# um Tabellen der Haeufigkeiten und Prozente zu erstellen
gen.x.possor.beob<-table(GENITIV, POSSESSOR)
gen.x.possor.proz<-prop.table(gen.x.possor.beob, 2)
# um einen Barplot zu erstellen
barplot(gen.x.possor.proz, beside=T, col=c("darkblue", "orange"), xlab="Possessor", ylab="Prozent", ylim=c(0, 1.05))
# um eine Legende hinzuzufuegen
legend(5, 0.92, c("of-Konstruktion", "s-Genitiv"), c("darkblue", "orange"), xjust=0.5, yjust=0.5, ncol=1, bty="n")
# um eine Beschriftung mit Zahlen hinzuzufuegen
text(c(1.5, 2.5, 4.5, 5.5, 7.5, 8.5), gen.x.possor.proz+0.03, paste(round(100*gen.x.possor.proz, 1), "%\n(", gen.x.possor.beob, ")", sep=""), cex=0.75) 

source("G:/r-workshop_data/_scripts/hcfa_3-2.r")


source('~/Documents/R_Workshop_Apr_2014/r-workshop_data/_scripts/hcfa_3-2.r', chdir = TRUE)

hcfa()








